# php-extended/php-record-assignable-logger
A library that implements php-record-assignable-interface library for logging purposes.

![coverage](https://gitlab.com/php-extended/php-record-assignable-logger/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-record-assignable-logger/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-record-assignable-logger ^8`


## Basic Usage

This library may be used the following way :

```php

use PhpExtended\Record\AssignableRecordLogger;

/* @var $record \PhpExtended\Record\AssignableRecordInterface */
/* @var $logger \Psr\Log\LoggerInterface */

$record = new AssignableRecordInterface($record, $logger);

```

And for the record provider :

```php

use PhpExtended\Record\AssignableRecordProviderLogger;

/* @var $provider \PhpExtended\Record\AssignableRecordProviderInterface */
/* @var $logger \Psr\Log\LoggerInterface */

$provider = new AssignableRecordProviderLogger($provider, $logger);

```


## License

MIT (See [license file](LICENSE)).
