<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-record-assignable-logger library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Record;

use Psr\Log\LoggerInterface;

/**
 * AssignableRecordLogger class file.
 * 
 * This class is an assignable record that logs modifying calls.
 * 
 * @author Anastaszor
 */
class AssignableRecordLogger implements AssignableRecordInterface
{
	
	/**
	 * The record.
	 * 
	 * @var AssignableRecordInterface
	 */
	protected AssignableRecordInterface $_record;
	
	/**
	 * The logger.
	 * 
	 * @var LoggerInterface
	 */
	protected LoggerInterface $_logger;
	
	/**
	 * Builds a new AssignableRecordLogger with the given record and logger.
	 * 
	 * @param AssignableRecordInterface $record
	 * @param LoggerInterface $logger
	 */
	public function __construct(AssignableRecordInterface $record, LoggerInterface $logger)
	{
		$this->_record = $record;
		$this->_logger = $logger;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Record\RecordInterface::getNamespace()
	 */
	public function getNamespace() : string
	{
		return $this->_record->getNamespace();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Record\RecordInterface::getClassname()
	 */
	public function getClassname() : string
	{
		return $this->_record->getClassname();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Record\RecordInterface::getIdentifier()
	 */
	public function getIdentifier() : string
	{
		return $this->_record->getIdentifier();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Record\RecordInterface::isAllowed()
	 */
	public function isAllowed(string $key) : bool
	{
		return $this->_record->isAllowed($key);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Record\RecordInterface::hasValue()
	 */
	public function hasValue(string $key) : bool
	{
		return $this->_record->hasValue($key);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Record\RecordInterface::getValue()
	 */
	public function getValue(string $key) : string
	{
		return $this->_record->getValue($key);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Record\RecordInterface::setValue()
	 */
	public function setValue(string $key, string $value) : bool
	{
		$this->_logger->info(
			'Setting {this}->{key} with value {value}',
			['this' => \get_class($this->_record), 'key' => $key, 'value' => $value],
		);
		
		return $this->_record->setValue($key, $value);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Assignable\AssignableInterface::isAssigned()
	 */
	public function isAssigned() : bool
	{
		return $this->_record->isAssigned();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Assignable\AssignableInterface::getAssignmentId()
	 */
	public function getAssignmentId() : string
	{
		return $this->_record->getAssignmentId();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Assignable\AssignableInterface::assign()
	 */
	public function assign(string $assignedId) : bool
	{
		$this->_logger->info(
			'Assigning {this} to key {key}',
			['this' => \get_class($this->_record), 'key' => $assignedId],
		);
		
		return $this->_record->assign($assignedId);
	}
	
}
