<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-record-assignable-logger library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Record;

use Iterator;
use Psr\Log\LoggerInterface;

/**
 * AssignableRecordProviderLogger class file.
 * 
 * This class is a record provider for assignable records that logs fetching calls.
 * 
 * @author Anastaszor
 */
class AssignableRecordProviderLogger extends RecordProviderLogger implements AssignableRecordProviderInterface
{
	
	/**
	 * The provider.
	 *
	 * @var AssignableRecordProviderInterface
	 */
	protected AssignableRecordProviderInterface $_aprovider;
	
	/**
	 * Builds a new AssignableRecordProviderLogger with the given provider and
	 * logger.
	 * 
	 * @param AssignableRecordProviderInterface $provider
	 * @param LoggerInterface $logger
	 */
	public function __construct(AssignableRecordProviderInterface $provider, LoggerInterface $logger)
	{
		parent::__construct($provider, $logger);
		$this->_aprovider = $provider;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Record\AssignableRecordProviderInterface::getAllAssignedRecords()
	 */
	public function getAllAssignedRecords(?string $namespace = null, ?string $classname = null) : Iterator
	{
		$this->_logger->info(
			'Retrieving all assigned records {ns}\\{cls}',
			['ns' => $namespace, 'cls' => $classname],
		);
		
		return $this->_aprovider->getAllAssignedRecords($namespace, $classname);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Record\AssignableRecordProviderInterface::getAllUnassignedRecords()
	 */
	public function getAllUnassignedRecords(?string $namespace = null, ?string $classname = null) : Iterator
	{
		$this->_logger->info(
			'Retrieving all unassigned records {ns}\\{cls}',
			['ns' => $namespace, 'cls' => $classname],
		);
		
		return $this->_aprovider->getAllUnassignedRecords($namespace, $classname);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Record\AssignableRecordProviderInterface::getAssignedRecords()
	 */
	public function getAssignedRecords(string $namespace, string $classname, string $assignmendId) : Iterator
	{
		$this->_logger->info(
			'Retrieving assigned records {ns}\\{cls} with assigned id {id}',
			['ns' => $namespace, 'cls' => $classname, 'id' => $assignmendId],
		);
		
		return $this->_aprovider->getAssignedRecords($namespace, $classname, $assignmendId);
	}
	
}
